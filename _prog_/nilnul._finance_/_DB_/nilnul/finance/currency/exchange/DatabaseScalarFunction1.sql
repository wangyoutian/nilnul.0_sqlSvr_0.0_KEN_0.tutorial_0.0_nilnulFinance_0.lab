﻿CREATE FUNCTION [nilnul.finance.currency].[Exchange]
(
	@amount money
	,
	@rate money
)
RETURNS money
AS
BEGIN
	RETURN @amount *@rate ;
END
