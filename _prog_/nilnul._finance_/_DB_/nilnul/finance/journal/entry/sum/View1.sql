﻿CREATE VIEW [nilnul.finance.journal.entry].[Sum]
	AS
select 
	max(id) latestId
	,
	entity 
	,
	sum(quantity) as [sum]
	,
	quality

	from [nilnul.finance.journal].[Entry]
	group by 
		entity
		, 
		quality 
