﻿CREATE FUNCTION [nilnul.finance.journal.entry.sum].[of_Entity]
(
	@entity nvarchar(4000)
)
RETURNS TABLE 
AS
RETURN (
	SELECT 
		[sum], [quality]
		from [nilnul.finance.journal.entry].[Sum] as s
		where s.entity = @entity
);
