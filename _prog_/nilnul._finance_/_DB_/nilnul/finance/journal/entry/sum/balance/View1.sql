﻿CREATE VIEW [nilnul.finance.journal.entry.sum].[Balance]
	AS
	select 
		quality
		,
		sum( [sum] ) balance
	from [nilnul.finance.journal.entry].[Sum]
	group by quality


