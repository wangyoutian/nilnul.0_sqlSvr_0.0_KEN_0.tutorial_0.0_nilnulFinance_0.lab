﻿CREATE FUNCTION [nilnul.finance.journal.entry.sum.balance].[CheckedSum]
(
	@entity nvarchar(4000)
)
RETURNS  @r TABLE(
	[quality] nvarchar(4000)
	,
	[sum] money
)
AS
begin 
	-- first we need to check whether our book is balanced
	if exists(
		select null  
			from 
				[nilnul.finance.journal.entry.sum].Balance
			 where [balance]!=0
	) begin
		insert @r(
			[quality]
			,
			[sum]
		)values(
			N'---'
			,null
		)
	end
	else begin
		insert @r(
			[quality]
			,
			[sum]
		)
			SELECT 
				 [quality],[sum]
				from [nilnul.finance.journal.entry].[Sum] as s
				where s.entity = @entity
	end
	return;
end