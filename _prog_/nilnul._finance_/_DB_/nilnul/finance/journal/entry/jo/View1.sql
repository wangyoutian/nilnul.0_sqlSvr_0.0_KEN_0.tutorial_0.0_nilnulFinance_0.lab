﻿CREATE VIEW 
	[nilnul.finance.journal.entry].[Jo]
	AS 
select 
	j.id
	,
	j.note journalNote
	,
	j.time journalTime 
	,e.id as entryId
	,e.entity
	,e.quantity
	,e.quality,
	e._memo entry_memo
	,e._time entry_time
	from 
		[nilnul.finance].[Journal] as j 
		 join
			[nilnul.finance.journal].[Entry] as e
				on j.id = e.journal
	
