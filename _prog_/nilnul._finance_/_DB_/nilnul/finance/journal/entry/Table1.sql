﻿CREATE TABLE [nilnul.finance.journal].[Entry]
(
	[id] BIGINT NOT NULL PRIMARY KEY identity
	, 
    [journal] BIGINT NOT NULL references [nilnul.finance].[Journal](id)
	,
	[entity] nvarchar(4000) not null    --such as cash or bank account
	,
	[quantity] money 
	,
	[quality] nvarchar(400) default N'cny' --chinese  yuan
	,
	_memo nvarchar(max)
	,
	_time datetime default getUtcDate()

)
