﻿CREATE TABLE [nilnul.finance].[Journal]
(
	[id] BIGINT NOT NULL PRIMARY KEY identity, 
    [note] NVARCHAR(4000) NOT NULL, 
    [time] DATETIME NOT NULL DEFAULT getUtcDate()

)
