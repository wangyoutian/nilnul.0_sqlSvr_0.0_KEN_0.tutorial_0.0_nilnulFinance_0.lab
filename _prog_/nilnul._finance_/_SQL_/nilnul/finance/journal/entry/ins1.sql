﻿declare @id bigint; --journal id
set @id =null;  --later change this to a non-exist value or null to see 
insert [nilnul.finance.journal].[Entry]
	(journal, entity, quantity, quality)
	output inserted.*
	values
	(
		@id, 
		N'cash'
		,
		-10
		,
		N'cny'
	)
	,
	(
		@id, 
		N'expense'
		,
		10
		,
		N'cny'
	)
;