﻿select 
	j.id
	,
	j.note
	,
	j.time
	,e.id as entryId
	,e.entity
	,e.quantity
	,e.quality,
	e._memo
	,e._time
	from 
		[nilnul.finance].[Journal] as j 
		 join
			[nilnul.finance.journal].[Entry] as e
				on j.id = e.journal
