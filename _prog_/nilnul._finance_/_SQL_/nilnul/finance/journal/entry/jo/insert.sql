﻿declare @ids [nilnul].[Id];  --  int i;  //  


insert [nilnul.finance].[Journal]
				([note])
	output inserted.id into @ids(id)
	output inserted.*
	values(
		N'购买苹果2个'
	)
; 

declare @id bigint = (
	select top 1 id 
		from @ids
);
-- int i=1;


insert [nilnul.finance.journal].[Entry]
	(journal, entity, quantity, quality)
	output inserted.*
	values
	(
		@id, 
		N'cash'
		,
		-10
		,
		N'cny'
	)
	,
	(
		@id, 
		N'expense'
		,
		10
		,
		N'cny'
	)
;