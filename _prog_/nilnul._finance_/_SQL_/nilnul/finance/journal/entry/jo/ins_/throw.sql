﻿--set xact_abort on;

declare @ids [nilnul].[Id];
insert [nilnul.finance].[Journal]
	([note])
	output inserted.id into @ids(id)
	output inserted.*
	values(
		N'购买苹果2个'
	)
; 
--throw 5000, N'err', 0;
raiserror ( N'err', 20, 0 )   with log
;
declare @id bigint = (
	select top 1 id 
		from @ids
);

insert [nilnul.finance.journal].[Entry]
	(journal, entity, quantity, quality)
	values
		(
		@id, 
		N'cash'
		,
		-10
		,
		N'cny'
	)
	,
	(
		@id, 
		N'expense'
		,
		10
		,
		N'cny'
	)
;
		 