﻿set xact_abort on;  --transact
begin tran  --saction  -- transaction
begin try
	declare @ids [nilnul].[Id];
	insert [nilnul.finance].[Journal]
		([note])
		output inserted.id into @ids(id)
		output inserted.*
		values(
			N'补记期初现金'
		)
	; 
	--throw 5000, N'err', 0;
	--raiserror ( N'err', 20, 0 )   with log
	--;
	declare @id bigint = (
		select top 1 id 
			from @ids
	);

	insert [nilnul.finance.journal].[Entry]
		(journal, entity, quantity, quality)
		output inserted.*
		values
			(
			@id, 
			N'cash'
			,
			200
			,
			N'cny'
		)
		,
		(
			@id, 
			N'equity'
			,
			-200
			,
			N'cny'
		)
	;
	commit; 		 
end try
begin catch 
	rollback;
	print error_message();
end catch

